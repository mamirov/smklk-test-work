package com.mamirov.smklk.dao;

import com.mamirov.smklk.model.Track;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrackRepository extends JpaRepository<Track, Long> {
}
