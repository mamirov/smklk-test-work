package com.mamirov.smklk.dao;

import com.mamirov.smklk.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Long> {
}
