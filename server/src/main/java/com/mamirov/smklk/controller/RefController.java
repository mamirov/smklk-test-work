package com.mamirov.smklk.controller;

import com.mamirov.smklk.model.LengthUnit;
import com.mamirov.smklk.model.SpeedUnit;
import com.mamirov.smklk.model.Transmission;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping(value = "/api/v1/refs")
public class RefController {

    @RequestMapping(method = RequestMethod.GET, value = "/transmissions")
    public Transmission[] getTransmissionList() {
        return Transmission.values();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/speedUnits")
    public SpeedUnit[] getSpeedUnitList() {
        return SpeedUnit.values();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/lengthUnits")
    public LengthUnit[] getLengthUnitList() {
        return LengthUnit.values();
    }
}
