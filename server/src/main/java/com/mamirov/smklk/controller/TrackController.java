package com.mamirov.smklk.controller;

import com.mamirov.smklk.model.*;
import com.mamirov.smklk.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/track")
public class TrackController {

    @Autowired
    private TrackService trackService;

    @RequestMapping(method = RequestMethod.GET)
    @CrossOrigin
    public TrackView getTrackList() {
        return new TrackView(trackService.findAll());
    }

    @RequestMapping(method = RequestMethod.POST)
    @CrossOrigin
    public TrackView save(@RequestBody TrackView trackView) {
        return new TrackView(trackService.massSave(trackView.getTracks()));
    }
}
