package com.mamirov.smklk.kernel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = "com.mamirov.smklk.model")
@EnableJpaRepositories(basePackages = "com.mamirov.smklk.dao")
@ComponentScan(basePackages = "com.mamirov.smklk")
public class Application {

    public static void main(String [] args) {
        SpringApplication.run(Application.class, args);
    }
}
