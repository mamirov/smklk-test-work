package com.mamirov.smklk.model;

public enum LengthUnit {
    KM("km"), M("m");

    private String name;

    LengthUnit(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return name;
    }
}
