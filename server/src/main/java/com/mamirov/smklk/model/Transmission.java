package com.mamirov.smklk.model;

public enum Transmission {
    AUTO("automatic"), MANUAL("manual");

    private String name;

    Transmission(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
