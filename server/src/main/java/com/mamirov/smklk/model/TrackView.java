package com.mamirov.smklk.model;

import java.util.List;

public class TrackView {

    public TrackView() {
    }

    public TrackView(List<Track> tracks) {
        this.tracks = tracks;
    }

    private List<Track> tracks;

    public List<Track> getTracks() {
        return tracks;
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
    }
}
