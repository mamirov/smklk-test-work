package com.mamirov.smklk.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(schema = "smklk", name = "cars")
public class Car {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "transmission")
    private Transmission transmission;

    @Column(name = "ai")
    private String ai;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "car_max_speed_id")
    private CarMaxSpeed maxSpeed;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    public String getAi() {
        return ai;
    }

    public void setAi(String ai) {
        this.ai = ai;
    }

    public CarMaxSpeed getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(CarMaxSpeed maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
}
