package com.mamirov.smklk.model;

public enum SpeedUnit {
    MPS("mps"), KMPS("kmps");

    private String name;

    SpeedUnit(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
