package com.mamirov.smklk.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(schema = "smklk", name = "tracks_length")
public class TrackLength {

    public TrackLength() {
    }

    public TrackLength(LengthUnit unit, BigDecimal value) {
        this.unit = unit;
        this.value = value;
    }

    @Id
    @GeneratedValue
    @Column(name = "id")
    @JsonIgnore
    private Long id;

    @Column(name = "unit")
    private LengthUnit unit;

    @Column(name = "value")
    private BigDecimal value;

    public LengthUnit getUnit() {
        return unit;
    }

    public void setUnit(LengthUnit unit) {
        this.unit = unit;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
