package com.mamirov.smklk.service;

import com.mamirov.smklk.model.Track;

import java.util.List;

public interface TrackService {

    Track findById(Long id);
    List<Track> findAll();
    Track save(Track track);
    List<Track> massSave(List<Track> trackList);
}
