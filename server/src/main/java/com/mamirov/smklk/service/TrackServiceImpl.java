package com.mamirov.smklk.service;

import com.mamirov.smklk.dao.TrackRepository;
import com.mamirov.smklk.model.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrackServiceImpl implements TrackService {

    @Autowired
    private TrackRepository trackRepository;

    @Override
    public Track findById(Long id) {
        return trackRepository.findOne(id);
    }

    @Override
    public List<Track> findAll() {
        return trackRepository.findAll();
    }

    @Override
    public Track save(Track track) {
        return trackRepository.save(track);
    }

    @Override
    public List<Track> massSave(List<Track> trackList) {
        return trackRepository.save(trackList);
    }
}
