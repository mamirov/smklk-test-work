package com.mamirov.smklk.service;

import com.mamirov.smklk.model.Car;

import java.util.List;

public interface CarService {

    Car findById(Long id);
    List<Car> findAll();
    Car save(Car car);
}
