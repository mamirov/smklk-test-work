import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TrackView } from "../models/track.view";

@Injectable()
export class TrackService {
    constructor(private http: HttpClient) {
    }

    getTracks(): Observable<TrackView> {
        return this.http.get<TrackView>("/api/v1/track");
    }

    saveTracks(trackView : TrackView) : Observable<TrackView> {
        return this.http.post<TrackView>("/api/v1/track", trackView)
            .pipe();
    }
}