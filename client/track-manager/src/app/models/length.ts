enum LengthUnit {KM, M}
export class Length {
    constructor(
        public unit: LengthUnit = LengthUnit.KM,
        public value: number = 0
        ){}
}