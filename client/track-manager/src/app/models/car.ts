import { MaxSpeed } from './max.speed';

enum Transmission {AUTO, MANUAL};
export class Car {
    constructor(
        public id: number = undefined,
        public code: string = '',
        public ai: string = '',
        public transmission: Transmission = Transmission.MANUAL,
        public maxSpeed: MaxSpeed = new MaxSpeed()
        ){}
}