import { Track } from './track';

export class TrackView {
    constructor(
        public tracks: Array<Track>
        ){}
}