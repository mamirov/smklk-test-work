enum SpeedUnit {KMPS, MPS}
export class MaxSpeed {
    constructor(
        public unit: SpeedUnit = SpeedUnit.MPS,
        public value: number = 0
        ) {
    }
}